export default {
  "contentDatas" : [
    {
      "title" : "Title1",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 13:00"
    },
    {
      "title" : "Title2",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 14:00"
    },
    {
      "title" : "Title3",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 15:00"
    },
    {
      "title" : "Title4",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 16:00"
    },
    {
      "title" : "Title5",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 17:00"
    },
    {
      "title" : "Title6",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 18:00"
    },
    {
      "title" : "Title7",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 19:00"
    },
    {
      "title" : "Title8",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 20:00"
    },
    {
      "title" : "Title9",
      "content" : " I am attempting to include my meta information (mostly visible in the static .css and .js files) to my Nuxt application by adding them into nuxt.config.js. I am expecting all of my global meta tags (charset, keywords, etc) as well as my CSS to be loaded when I reload the project on the page I'm testing on, however only using the local vue-meta section gives these desired results.",
      "contentUpdateDate" : "28 July, 2019 21:00"
    }
  ]
}